<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class First extends Migration
{
    const COUNTRY_FIELDS = [
        'id' => [
            'type'           => 'SMALLINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'auto_increment' => true
        ],
        'code' => [
            'type' => 'CHAR',
            'constraint' => '2',
            'unique' => true,
            'null' => false
        ],
        'name' => [
            'type' => 'VARCHAR',
            'constraint' => '32',
            'unique' => true,
            'null' => false
        ],
        'lat' => [
            'type' => 'FLOAT',
            'constraint' => '10,6'
        ],
        'lng' => [
            'type' => 'FLOAT',
            'constraint' => '10,6'
        ],
    ];

    const AIRPORT_FIELDS = [
        'id' => [
            'type'           => 'MEDIUMINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'auto_increment' => true
        ],
        'name' => [
            'type'           => 'VARCHAR',
            'constraint'     => '100',
            'unique'         => true,
            'null' => false
        ],
        'lat' => [
            'type' => 'FLOAT',
            'constraint' => '10,6'
        ],
        'lng' => [
            'type' => 'FLOAT',
            'constraint' => '10,6'
        ],
        'country' => [
            'type'           => 'SMALLINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'null' => false
        ]
    ];

    const AIRLINES_FIELDS = [
        'id' => [
            'type'           => 'MEDIUMINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'auto_increment' => true
        ],
        'name' => [
            'type' => 'VARCHAR',
            'constraint' => '100',
            'unique' => true,
            'null' => false
        ],
        'country' => [
            'type'           => 'SMALLINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'null' => false
        ]
    ];

    const AIRPORT_AIRLINE_FIELDS = [
        'airport_id' => [
            'type'           => 'MEDIUMINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'null' => false
        ],
        'airline_id' => [
            'type'           => 'MEDIUMINT',
            'constraint'     => 5,
            'unsigned'       => true,
            'null' => false
        ]

    ];

    const CREATE_TABLE_ATTR = [
        'ENGINE' => 'InnoDB'
    ];

	public function up()
	{
        //Create country table
		$this->forge
            ->addField(self::COUNTRY_FIELDS)
            ->addKey('id', true)
            ->createTable('countries', true, self::CREATE_TABLE_ATTR);

        //Create airlines table
        $this->forge
            ->addField(self::AIRLINES_FIELDS)
            ->addKey('id', true)
            ->addForeignKey('country', 'countries', 'id')
            ->createTable('airlines', true, self::CREATE_TABLE_ATTR);

        //Create airport table
        $this->forge
            ->addField(self::AIRPORT_FIELDS)
            ->addKey('id', true)
            ->addForeignKey('country', 'countries', 'id')
            ->createTable('airports', true, self::CREATE_TABLE_ATTR);

        //Create airport-airlines table
        $this->forge
            ->addField(self::AIRPORT_AIRLINE_FIELDS)
            ->addForeignKey('airport_id', 'airports', 'id', '', 'CASCADE')
            ->addForeignKey('airline_id', 'airlines', 'id', '', 'CASCADE')
            ->addUniqueKey(['airport_id', 'airline_id'])
            ->createTable('airport_airlines', true, self::CREATE_TABLE_ATTR);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
