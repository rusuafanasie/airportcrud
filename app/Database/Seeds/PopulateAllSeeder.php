<?php


namespace App\Database\Seeds;


class PopulateAllSeeder extends \CodeIgniter\Database\Seeder
{
    public function run() {
        $countryID = $this->insertCountries();
        $airportID = $this->insertAirport($countryID);
        $airlineID = $this->insertAirlines($countryID);
        $this->insertAirportAirline($airportID, $airlineID);

        echo 'Database populated';
    }

    private function insertCountries() {
        $data = [
            [
                'name' => 'Germany',
                'code' => 'DE',
                'lat' => 51.038537,
                'lng' => 10.634095
            ],
            [
                'name' => 'Russia',
                'code' => 'RU',
                'lat' => 62.240279,
                'lng' => 99.535797
            ]
            ];

        $this->db
            ->table('countries')
            ->insertBatch($data);

        return $this->db->insertID();
    }

    private function insertAirport(int $countryId) {
        $data = [
            'name' => 'Frankfurt Airport',
            'lat'    => 50.037631,
            'lng'   => 8.561963,
            'country' => $countryId
        ];

        $this->db
            ->table('airports')
            ->insert($data);

        return $this->db->insertID();
    }

    private function insertAirlines(int $countryId) {
        $data = [[
            'name' => 'Lufthansa',
            'country' => $countryId
        ],
        [
            'name' => 'Aeroflot',
            'country' => $countryId
        ]
        ];

        $this->db
            ->table('airlines')
            ->insertBatch($data);

        return $this->db->insertID();
    }

    private function insertAirportAirline(int $airport, int $airline) {
        $data = [
            'airport_id' => $airport,
            'airline_id' => $airline
        ];

        $this->db
            ->table('airport_airlines')
            ->insert($data);
    }
}