<?php


namespace App\Commands;


use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;

class CreateDatabase extends BaseCommand
{
    protected $group = 'Database';
    protected $name = 'db:create';
    protected $description = 'Create database';

    /**
     * Actually execute a command.
     * This has to be over-ridden in any concrete implementation.
     *
     * @param array $params
     */
    public function run(array $params)
    {

        try {
            $forge = \Config\Database::forge();
            $forge->createDatabase('airportCRUD');
        } catch (\mysqli_sql_exception $exception) {
            $code = $exception->getCode();
            if ($code === 1045)
                return CLI::error('Database username or password is incorrect. Please verify the username and password in (app/Config/Database.php) or in .env file from project root');
            if ($code === 1007) {
                return CLI::error('Database exist. You can continue with migration');
            }
        }

        CLI::write('Database created. You can continue with migration', 'blue');
    }
}