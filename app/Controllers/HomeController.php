<?php namespace App\Controllers;

use CodeIgniter\View\Table;
use App\Models\AirportModel;

class HomeController extends BaseController
{
    const TABLE_TEMPLATE = [
        'table_open' => '<table class="table table-hover table-secondary">',
        'thead_open' => '<thead class="table-light">',
    ];

	public function index()
	{
	    $airport = new AirportModel();
	    $data = $airport->findAndCountries();

        $table = new Table(self::TABLE_TEMPLATE);
        $table->setHeading('Name', 'Country');

        if (count($data))
            foreach ($data as $val) {
                $table->addRow([
                    'data' => $val['name'],
                    'class' => 'airport-name',
                    'data-id' => $val['id']],
                    $val['country']);
            }
        else
            $table->addRow([
                'data' => 'No airports',
                ]
            );

        $table = $table->generate();

		return view('home', ['table' => $table]);
	}

	//--------------------------------------------------------------------

}
