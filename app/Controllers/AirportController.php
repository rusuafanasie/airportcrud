<?php


namespace App\Controllers;


use App\Models\AirlineModel;
use App\Models\AirportModel;
use App\Models\CountryModel;

class AirportController extends BaseController
{
    public function index() {
        $countryModel = new CountryModel();
        $airlineModel = new AirlineModel();
        $airportModel = new AirportModel();

        $countries = $countryModel->findAll();
        $airlineModel = $airlineModel->findAll();
        $airports = $airportModel->findAllIdAndName();

        return view('addUpdateAirport', [
            'countries' => $countries,
            'airlines' => $airlineModel,
            'airports' => $airports
        ]);
    }
}