<?php


namespace App\Controllers;


use App\Entities\Airport;
use App\Models\AirportAirlinesModel;
use CodeIgniter\RESTful\ResourceController;

class AirportAPI extends ResourceController
{
    protected $modelName = 'App\Models\AirportModel';

    public function getAllDetails(int $id) {
        $airport = $this->model->asArray()->find($id);
        $airlines = $this->model->findDetails($id);

        return $this->response
            ->setStatusCode(200)
            ->setJSON([
                'airport' => $airport,
                'airlines' => $airlines
            ]);
    }

    public function getDetails(int $id) {
        $id = intval($id);

        $airLines = $this->model->findDetails($id);
        $coordinates = $this->model->getCoordinates($id);

        return $this->response
            ->setStatusCode(200)
            ->setJSON(['details' => [
                'coordinates' => $coordinates,
                'airlines' => $airLines
            ]]);
    }

    public function create() {

        $data = $this->request->getJSON(true);

        $data['lat'] = floatval($data['lat']);
        $data['lng'] = floatval($data['lng']);

        $airport = new Airport();
        $airport->fill($data);

        //Validating and saving data if all the data are valid
        $airportInsertRes = $this->model->save($airport);


        if ($airportInsertRes) {
            $airlines = $data['airlines'];

            // Inserting the related many to many ID's Airport-Airlines if available
            if (is_array($airlines) &&  count($airlines)) {
                $airportAirlines = new AirportAirlinesModel();
                $relatedData = [];
                $airportNewID = $this->model->getInsertID();

                foreach ($airlines as $airline) {
                    $relatedData[] = [
                        'airport_id' => $airportNewID,
                        'airline_id' => $airline
                    ];
                }
                $airportAirlineInsertRes = $airportAirlines->insertBatch($relatedData);

                if (!$airportAirlineInsertRes)
                    return $this->response
                        ->setStatusCode(400)
                        ->setJSON(['airlines' => 'The airport is saved but related airlines are corrupt']);
            }

            return $this->response
                ->setStatusCode(201)
                ->setJSON(['airport' => 'created']);

        }

        return $this->response
            ->setStatusCode(400)
            ->setJSON($this->model->errors());
    }

    public function update($id = null) {
        $id = intval($id);

        if (!$id) return $this->response
            ->setStatusCode(400)
            ->setJSON(['airport' => 'Please select an airport to update it']);

        $data = $this->request->getJSON(true);
        $data['id'] = $id;

        //Validating and saving data if all the data are valid
        $airportInsertRes = $this->model->set($data)->where('id', $id)->update();


        if ($airportInsertRes) {
            $airlines = $data['airlines'];

            // Inserting the related many to many ID's Airport-Airlines if available
            if (is_array($airlines)) {
                $airportAirlines = new AirportAirlinesModel();

                $airportAirlineInsertRes = $airportAirlines->deleteInsertSafe($id, $airlines);

                if (!$airportAirlineInsertRes)
                    return $this->response
                        ->setStatusCode(400)
                        ->setJSON(['airlines' => 'The airport is saved but related airlines are corrupt']);
            }

            return $this->response
                ->setStatusCode(200)
                ->setJSON(['airport' => 'updated']);

        }

        return $this->response
            ->setStatusCode(400)
            ->setJSON($this->model->errors());
    }

    public function delete($id = null)
    {
        $id = intval($id);
        if (is_int($id)) {
            $deleted = $this->model->delete($id);
            if ($deleted) return $this->response
                ->setStatusCode(201)
                ->setJSON(['airport' => 'deleted']);
        }
    }
}