<?php namespace Config;

use CodeIgniter\Config\Services as CoreServices;
use CodeIgniter\Config\BaseConfig;
use CodeIgniter\HTTP\RequestInterface;

require_once SYSTEMPATH . 'Config/Services.php';

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends CoreServices
{
    const ERROR_MESSAGE = [
        'noAjax' => 'The request is not AJAX',
        'noJson' => 'The request body is not json or is corrupt',
        'media' => 'Unsupported media type'
    ];

    public static function validateRequest(RequestInterface $request) {
        if (!$request->isAJAX())
            return [
                'code' => 400,
                'message' => self::ERROR_MESSAGE['noAjax']
            ];

        if ($request->getHeader('content-type')->getValue() !== 'application/json')
            return [
                'code' => 415,
                'message' => self::ERROR_MESSAGE['media']
            ];

        $data = $request->getJSON(true);
        if ($data === null)
            return [
                'code' => 415,
                'message' => self::ERROR_MESSAGE['noJson']
            ];

        return true;
    }

	//    public static function example($getShared = true)
	//    {
	//        if ($getShared)
	//        {
	//            return static::getSharedInstance('example');
	//        }
	//
	//        return new \CodeIgniter\Example();
	//    }
}
