<?php


namespace App\Models;

use CodeIgniter\Model;


class AirportModel extends Model
{
    protected $table = 'airports';
    protected $allowedFields = [
        'name', 'country', 'location', 'lat','lng'
    ];
    protected $returnType = 'App\Entities\Airport';

    protected $validationRules = [
        'name' => 'required|is_unique[airports.name]',
        'country' => 'required',
        'lat' => 'required',
        'long' => 'required',
    ];

    protected $validationMessages = [
        'name' => [
            'is_unique' => 'Airport name exist in database. Please use a new name'
        ],
        'lat' => [
            'required' => 'The location is required. Use the google map to pin a mark'
        ],
        'long' => [
            'required' => 'The location is required. Use the google map to pin a mark'
        ]
    ];

    public function findDetails(int $id) {


        $airlines = $this->db
            ->query("SELECT AL.id, AL.name, C.name AS country
                FROM airlines AS AL
                INNER JOIN airport_airlines AS AA 
                ON AA.airline_id = AL.id AND AA.airport_id = $id
                INNER JOIN countries AS C ON C.id = AL.country")
            ->getResultArray();

        return $airlines;

    }

    public function getCoordinates(int $id) {
        return $this->db
            ->query("SELECT lat, lng FROM airports WHERE id = $id;")
            ->getRowArray();
    }

    public function findAndCountries() {
        $data = $this->db
            ->query('SELECT A.id, A.name, C.name AS country 
                FROM airports AS A 
                INNER JOIN countries AS C 
                ON A.country = C.id')
            ->getResultArray();

        return $data;
    }

    public function findAllIdAndName() {
        $data = $this->db
            ->query('SELECT id, name 
                FROM airports')
            ->getResultArray();

        return $data;
    }
}