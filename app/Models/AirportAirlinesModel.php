<?php


namespace App\Models;


use CodeIgniter\Model;

class AirportAirlinesModel extends Model
{
    protected $table = 'airport_airlines';
    protected $allowedFields = [
        'airport_id', 'airline_id'
    ];
    protected $returnType = 'App\Entities\AirportAirlines';

    public function deleteInsertSafe(int $airportID, array $airlines) {
        if (count($airlines)) {
            $db = $this->db;
            $exceptId = implode(',', $airlines);

            $db->transStart();
            $db->query(
                "DELETE FROM $this->table WHERE airport_id = $airportID AND airline_id NOT IN($exceptId);"
            );
            foreach ($airlines as $airline) {
                $db->query("REPLACE INTO $this->table(airport_id, airline_id) VALUE ($airportID, $airline);");
            }
            $db->transComplete();

            return $this->db->transStatus();

        } else return $this->where(['airport_id' => $airportID])->delete();

    }
}