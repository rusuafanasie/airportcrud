<?php


namespace App\Models;


use CodeIgniter\Model;

class AirlineModel extends Model
{
    protected $table = 'airlines';
    protected $allowedFields = [
        'name', 'country'
    ];

    /*protected $returnType = 'App\Entities\Airline';*/
}