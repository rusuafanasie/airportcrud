<div class="modal fade" tabindex="-1" role="dialog" id="detailModal">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center text-white"><h3>Details</h3></div>
            <div class="modal-body text-center">
                <div class="spinner-border" role="status">
                    <span class="sr-only"></span>
                </div>
                <div class="row" id="allDetails">
                    <div class="col-6">
                        <ul class="list-group text-left">
                        </ul>
                    </div>
                    <div class="col-6" id="googleMaps"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-toggle="modal" data-target="#detailModal">Close</button>
            </div>
        </div>
    </div>
</div>

