<!doctype html>
<html>
<head>
    <title>Welcome to airport manager</title>
    <link rel="stylesheet" type="text/css" href="/build/app.css"/>
    <script type="application/javascript" rel="script" src="/build/runtime.js"></script>
    <script type="application/javascript" rel="script" src="/build/app.js"></script>
    <?= $this->renderSection('headContent') ?>
</head>
<body>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-gradient-dark">
    <a class="navbar-brand" href="/">APManagement</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBarItems">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navBarItems">
        <div class="navbar-nav">
            <a class="nav-item nav-link <?= uri_string() === '/' ? 'active' : '' ?>" href="/">Home</a>
            <a class="nav-item nav-link <?= uri_string() === 'airport' ? 'active' : '' ?>" href="/airport">Add/Update Airport</a>
            <a class="nav-item nav-link disabled" href="#">Manage Airline</a>
        </div>
    </div>
</nav>
<div class="container bg-gradient-tertiary pt-3 pb-1">
    <?= $this->renderSection('content') ?>
</div>
<footer>

</footer>
    <?= $this->renderSection('script') ?>
</body>
</html>