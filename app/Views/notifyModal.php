<div class="modal fade" tabindex="-1" role="dialog" id="notifyModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header"></div>
            <div class="modal-body text-center">
                <div class="spinner-border text-center" role="status">
                    <span class="sr-only"></span>
                </div>
                <div class="row" id="notification">
                    <div class="col-12">
                        <span class="notify-message h1"></span>
                    </div>
                </div>
                <dl class="row text-left" id="notification-list"></dl>
                <div class="row" id="notificationHint">
                    <div class="col-12">
                        <small class="text-muted"></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-toggle="modal" data-target="#notifyModal">Close</button>
            </div>
        </div>
    </div>
</div>
