<?= $this->extend('layout') ?>

<?= $this->section('headContent') ?>
    <link rel="stylesheet" type="text/css" href="/build/addAirport.css"/>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?= view('notifyModal'); ?>
    <h2 class="text-center text-white">Manage Airport</h2>
    <p class="lead text-center text-white">Complete the form and press the Save button</p>
    <div class="jumbotron">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="createUpdate" id="newAirport" value="new" checked>
                    <label class="form-check-label" for="newAirport">New</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="createUpdate" id="updateAirport" value="update">
                    <label class="form-check-label" for="updateAirport">Update</label>
                </div>
            </div>
        </div>
        <form>
            <input type="hidden" name="lat">
            <input type="hidden" name="lng">
            <div class="form-row">
                <div class="form-group col-md-4" id="forNew">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group col-md-4 d-none" id="forUpdate">
                    <label for="airport">Airport</label>
                    <select id="airport" class="form-control" name="id">
                        <option value="" selected>Choose...</option>
                        <?php foreach ($airports as $airport):?>
                            <option value="<?= $airport['id'] ?>"><?= $airport['name'] ?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="country">Country</label>
                    <select id="country" class="form-control" name="country">
                        <option value="" selected>Choose...</option>
                        <?php foreach ($countries as $country):?>
                        <option data-lat="<?= $country['lat'] ?>" data-lng="<?= $country['lng'] ?>" value="<?= $country['id'] ?>"><?= $country['name'] ?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group col-md-5">
                    <label for="airlines">Airlines</label>
                    <select id="airlines" class="selectpicker form-control" multiple>
                        <?php foreach ($airlines as $airline):?>
                            <option value="<?= $airline['id'] ?>"><?= $airline['name'] ?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12" id="googleMaps" style="height: 300px"></div>
            </div>
            <button type="submit" class="btn btn-secondary">Save</button>
            <div class="card mt-3">
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3 col-lg-3">Google Maps</dt>
                        <dd class="col-sm-9 col-lg-9">Click on the map to put a mark on it. Right click delete the mark</dd>
                    </dl>
                </div>
            </div>
        </form>
    </div>
<?= $this->endSection() ?>
<?= $this->section('script') ?>
    <script type="application/javascript" rel="script" src="/build/airport.js"></script>
    <script type="application/javascript" rel="script" src="/build/notifyModal.js"></script>
<?= $this->endSection() ?>