<?= $this->extend('layout') ?>

<?= $this->section('headContent') ?>
    <link rel="stylesheet" type="text/css" href="/build/home.css"/>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<?= view('detailsModal') ?>
<h2 class="text-center text-white">Airport List</h2>
<p class="lead text-center text-white">Please click on any airport to show more details</p>
<?= $table ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script type="application/javascript" rel="script" src="/build/homePage.js"></script>
    <script type="application/javascript" rel="script" src="/build/detailsModal.js"></script>
<?= $this->endSection() ?>