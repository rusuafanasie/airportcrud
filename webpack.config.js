const Encore = require('@symfony/webpack-encore');

Encore
    .configureRuntimeEnvironment('dev')
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './src/index.js')
    .addEntry('airport', './src/airport.js')
    .addEntry('notifyModal', './src/notifyModal.js')
    .addEntry('homePage', './src/home.js')
    .addEntry('detailsModal', './src/detailsModal.js')
    .addStyleEntry('home', './src/scss/home.scss')
    .addStyleEntry('addAirport', './src/scss/addAirport.scss')
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSassLoader()
    .autoProvidejQuery();

module.exports = Encore.getWebpackConfig();