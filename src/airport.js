import 'bootstrap-select';
import {
    showNotifyModal,
    setNotifyModalMessage
} from './notifyModal';
import Map from './mapsConfig';

/*Google Maps Setting*/
var map, mark, googleM;
var latInput = document.querySelector('input[name=lat]');
var lngInput = document.querySelector('input[name=lng]');
Map.loadGoogleMapsApi().then(function(googleMaps) {
    googleM = googleMaps;
    map = Map.createMap(googleMaps, document.getElementById('googleMaps'));
    map.addListener('click', function (e) {
        if (mark) {
            mark.setPosition(e.latLng);
            mark.setMap(map);
        }
        else mark = new googleMaps.Marker({position: e.latLng, map: map});

        latInput.value = e.latLng.lat();
        lngInput.value = e.latLng.lng();
    });
    map.addListener('rightclick', function () {
        if (mark) mark.setMap(null);
        lngInput.value = latInput.value = "";
    });
});
//---------------------------------------------------------------------

var mainForm = $('form');
var inputNew = mainForm.find('#forNew');
var inputUpdate = mainForm.find('#forUpdate');

// This function compute between creating a new airport or update an airport
$('input[name=createUpdate]').change(function(e) {

    if (e.target.value === 'new') {
        inputNew.removeClass('d-none');
        inputUpdate.addClass('d-none');
    } else if (e.target.value === 'update') {
        inputNew.addClass('d-none');
        inputUpdate.removeClass('d-none');
    }
    refreshInputs();
});

//When change country coordinates for the country are set in maps
mainForm.find('select[name=country]').change(function (e) {
    var option = this.querySelector('option[value*="' + e.target.value + '"]');
    if (e.target.value) {
        map.setCenter({
            lat: parseFloat(option.dataset.lat),
            lng: parseFloat(option.dataset.lng)
        });
        mark.setMap(null);
    }
});

//Submit data
mainForm.submit(function (e) {
    e.preventDefault();

    showNotifyModal();

    var currentState = $('input[name=createUpdate]:checked').val();
    var airlines = $(this).find('#airlines').val();

    var ajaxSettings = {
        contentType: 'application/json',
        dataType: 'json',
    };

    var data = {
        'country': this.querySelector('select[name=country]').value,
        'lat': latInput.value,
        'lng': lngInput.value,
        'airlines': airlines
    };

    if (currentState === 'new') {
        ajaxSettings.url = '/airport/create';
        ajaxSettings.method = 'POST';

        data.name = this.querySelector('input[name=name]').value;
    } else if (currentState === 'update') {
        var airportId = this.querySelector('select[name=id]').value;
        airportId = airportId ? airportId : 0;

        ajaxSettings.url = '/airport/update/' + airportId;
        ajaxSettings.method = 'PUT';
    }

    ajaxSettings.data = JSON.stringify(data);

    $.ajax(ajaxSettings)
        .done(function (data) {
            refreshInputs();
            setNotifyModalMessage(
                'Airport ' + data.airport,
                'To see the new airport please navigate to home page',
                'Success',
                'success');
            }).fail(function (data) {
                setNotifyModalMessage(data.responseJSON,
                'Close the modal and please solve the issues',
                'Issues',
                'danger')
    });
});

// When to update data. 
// If you change then name of airport from select dropdown, the data will be populated with actual data
mainForm.find('#airport').change(function () {
    if (this.value) {
        $.ajax({
            contentType: 'application/json',
            dataType: 'json',
            url: '/airport/details/all/' + this.value,
            method: 'GET'
        }).done(function (data) {

            var airport = data.airport;
            var airlinesValues = [];
            mainForm.find('select[name=country]').val(airport.country);

            data.airlines.forEach(function (airline) {
                airlinesValues.push(airline.id);
            });
            mainForm.find('.selectpicker').selectpicker('val', airlinesValues);

            var coordinates = {
                lat: parseFloat(airport.lat),
                lng: parseFloat(airport.lng)
            };
            map.setCenter(coordinates);
            if (mark) {
                mark.setPosition(coordinates);
                mark.setMap(map);
            } else mark = new googleM.Marker({position: coordinates, map: map});
        })
    } else refreshInputs();
});

function refreshInputs() {
    mainForm.find('input, select').val("");
    mainForm.find('.selectpicker').selectpicker('deselectAll').selectpicker('refresh');
    if (mark) mark.setMap(null);
}