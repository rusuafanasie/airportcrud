const googleMapsApi = require('load-google-maps-api');
class Map {

    static loadGoogleMapsApi() {
        return googleMapsApi({ key: 'AIzaSyBeMwX0mfZEBoLe6Pgctc4wwjtRnmsGNwI' });
    }
    static createMap(googleMaps, mapElement, coordinates) {
        coordinates = coordinates ? coordinates : { lat: 51.038537, lng: 10.634095 };
        return new googleMaps.Map(mapElement, {
            center: coordinates,
            zoom: 6
        });
    }
}
export default Map;