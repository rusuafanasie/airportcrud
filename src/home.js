import {
   showDetailsModal,
   setDetailsToModal
} from './detailsModal';

var table = $('table');
table.find('tr').click(function (e) {
   var id = e.currentTarget.getElementsByClassName('airport-name')[0].dataset.id;
   showDetailsModal();

   $.ajax({
      contentType: 'application/json',
      dataType: 'json',
      url: '/airport/details/' + id,
      method: 'GET'
   }).done(function (data) {
      if (data.details) setDetailsToModal(data.details);
   }).fail(function (data) {

   });
});