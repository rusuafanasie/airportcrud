import Map from './mapsConfig';

var modal = $('#detailModal');
var map, mark;

modal.on('show.bs.modal', function () {
    modal.find('.modal-header, .modal-footer, #allDetails').addClass('d-none');
    modal.find('.spinner-border').removeClass('d-none');
});

export function showDetailsModal() {
    modal.modal('show');
}

export function setDetailsToModal(details) {
    var coordinates = {
        lat: parseFloat(details.coordinates.lat),
        lng: parseFloat(details.coordinates.lng)
    };

    if(!map) map = Map.loadGoogleMapsApi().then(function(googleMaps) {
        var mapsDiv = modal.find('#googleMaps');

        mapsDiv.css('height', '300px');
        map = Map.createMap(googleMaps, mapsDiv[0], coordinates);
        mark = new googleMaps.Marker({position: coordinates, map: map});
        setAirlines(details.airlines);
        hideLoader();
    }); else {
        map.setCenter(coordinates);
        mark.setPosition(coordinates);
        setAirlines(details.airlines);
        hideLoader();
    }
}

function setAirlines(airlines) {
    var listUl = modal.find('#allDetails .list-group');
    listUl.find('li').remove();
    if (Array.isArray(airlines) && airlines.length)
        airlines.forEach(function (airline) {
            listUl.append('<li class="list-group-item"><h5>' + airline.name + '</h5><small class="text-muted lead">' + airline.country + '</small></li>');
        });
    else listUl.append('<li class="list-group-item">No airlines</li>');

};

function hideLoader() {
    modal.find('.spinner-border').addClass('d-none');
    modal.find('.modal-header, .modal-footer, #allDetails').removeClass('d-none');
}