var modal = $('#notifyModal');
/*modal.on('hidden.bs.modal', function () {


    modal.find('#notification').addClass('d-none');
    modal.find('.spinner-border').removeClass('d-none');
});*/

modal.on('show.bs.modal', function () {
    modal
        .find('#notification, #notificationHint, .modal-header, .modal-footer, #notification-list')
        .addClass('d-none');
    modal.find('.spinner-border').removeClass('d-none');
});

export function showNotifyModal() {
    modal.modal('show');
}

/**
 *
 * @param message - The message in modal body (Array|String)
 * @param hintMessage - This is a message for what client should do next (String)
 * @param headerTitle - Modal header title (String)
 * @param type - this can be one of the bootstrap type (success, warning, info, danger)
 */

export function setNotifyModalMessage(message, hintMessage, headerTitle, type) {
    var modalHeader = modal.find('.modal-header');
    modalHeader.children().remove();
    modalHeader.removeClass().addClass('modal-header bg-' + type)
        .append('<div class="display-4 text-white">' + headerTitle + '</div>');

    var hint = modal.find('#notificationHint');
    hint.find('.text-muted').text(hintMessage);
    hint.removeClass('d-none');


    if (typeof message === 'string') {
        modal.find('.notify-message').text(message);
        modal.find('#notification, .modal-footer').removeClass('d-none');
        modal.find('.spinner-border').addClass('d-none');
    } else if (typeof message === 'object' && typeof message !== 'string') {
        var notifyList = modal.find('#notification-list');


        notifyList.children().remove();
        for (var errorSource in message) {
            notifyList.append(
                '<dt class="col-2">' + errorSource + '</dt>'
                + '<dd class="col-10">' + message[errorSource] + '</dd>'
            )
        }

        modal.find('.spinner-border, #notification').addClass('d-none');
        modal.find('.modal-header, .modal-footer, #notification-list').removeClass('d-none');
    }
}