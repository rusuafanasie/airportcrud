# README #

To run this app the following technologies bust be installed: 
- NPM
- PHP
- MySql
- Apache


### How do I get set up? ###
#### 1. The application is using Apache Web Server ####

The virtual host is the following: 

    ServerName airportcrud
    DocumentRoot "[pathOfTheProject]/public"
    <Directory  "[pathOfTheProject]/public/">
        Options +Indexes +Includes +FollowSymLinks +MultiViews
        AllowOverride All
        Require local
    </Directory>
    
#### 2. Config the app (Database) ####

The config file is in `app/Config/Database.php`:

`app/Config/Database.php`:

    public $default = [
            'DSN'      => '',
            'hostname' => 'localhost',
    --->    'username' => '',
    --->    'password' => '',
    --->    'database' => '',
            'DBDriver' => 'MySQLi',
            'DBPrefix' => '',
            'pConnect' => false,
            'DBDebug'  => (ENVIRONMENT !== 'production'),
            'cacheOn'  => false,
            'cacheDir' => '',
            'charset'  => 'utf8',
            'DBCollat' => 'utf8_general_ci',
            'swapPre'  => '',
            'encrypt'  => false,
            'compress' => false,
            'strictOn' => false,
            'failover' => [],
            'port'     => 3306,
        ];

Then please create the database group in database also by using this name `airportCRUD` with collat `utf8_general_ci`.

Navigate with terminal to project root an run the following commands:
- `php spark migrate` - This will create database schema
- `php spark db:seed PopulateAllSeeder` - This will populate with data

The database is set

#### 2. Config the app (Libraries) ####

The following command will install the libraries:

- `npm install` - run this from project root

Next run the next command for compiling scss/js:

- `npm run dev` or `npm run prod`

*And this is all. The application must work*